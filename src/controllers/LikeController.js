const LikeService = require('../services/LikeService');

module.exports = (router) => {
  const url = '/likes';

  router.post(`${url}/like/:id`, LikeService.like);
  router.post(`${url}/unlike/:id`, LikeService.unlike);
};
