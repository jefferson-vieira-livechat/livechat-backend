const TweetService = require('../services/TweetService');

module.exports = (router) => {
  const url = '/tweets';

  router.get(url, TweetService.findAll);
  router.post(url, TweetService.save);
};
