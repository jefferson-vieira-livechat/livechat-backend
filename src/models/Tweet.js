const mongoose = require('mongoose');

const removeExtraSpaces = require('../utils/removeExtraSpaces');

const TweetSchema = new mongoose.Schema({
  author: {
    type: String,
    required: [true, 'Informe um nome!'],
    set: removeExtraSpaces,
  },
  content: {
    type: String,
    required: [true, 'Envie alguma mensagem!'],
    set: removeExtraSpaces,
  },
  likes: {
    type: Number,
    default: 0,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model('Tweet', TweetSchema);
