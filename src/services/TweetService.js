const Tweet = require('../models/Tweet');

const handleError = require('../utils/handleError');

async function findAll(req, res) {
  try {
    const tweets = await Tweet.find({}).sort('-createdAt');

    return res.json(tweets);
  } catch (error) {
    return res.status(500).end();
  }
}

async function save(req, res) {
  try {
    const tweet = await Tweet.create(req.body);

    req.io.emit('tweet', tweet);

    return res.json(tweet);
  } catch (error) {
    return res.status(400).send(handleError(error));
  }
}

module.exports = {
  findAll,
  save,
};
