const Tweet = require('../models/Tweet');

async function like(req, res) {
  try {
    const tweet = await Tweet.findById(req.params.id);

    tweet.set({ likes: tweet.likes + 1 });

    await tweet.save();

    req.io.emit('like', tweet);

    return res.json(tweet);
  } catch (error) {
    return res.staus(500).end();
  }
}

async function unlike(req, res) {
  try {
    const tweet = await Tweet.findById(req.params.id);

    tweet.set({ likes: tweet.likes - 1 });

    await tweet.save();

    req.io.emit('unlike', tweet);

    return res.json(tweet);
  } catch (error) {
    return res.status(500).end();
  }
}

module.exports = {
  like,
  unlike,
};
