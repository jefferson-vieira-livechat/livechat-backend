const _ = require('lodash');

module.exports = (err) => {
  const errors = [];
  _.forIn(err.errors, error => errors.push(error.message));
  return { messages: errors };
};
