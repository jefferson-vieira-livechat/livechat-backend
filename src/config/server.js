const port = 3000;

const express = require('express');
const http = require('http');

const socket = require('./socket');
const allowCors = require('./cors');

const server = express();

server.use(express.json());
server.use(socket(http.Server(server)));
server.use(allowCors);

server.listen(port);

module.exports = server;
