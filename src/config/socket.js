const socket = require('socket.io');

module.exports = server => (req, res, next) => {
  const io = socket(server);
  req.io = io;

  next();
};
