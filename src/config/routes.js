const express = require('express');

const routes = express.Router();

require('../controllers/TweetContrller')(routes);
require('../controllers/LikeController')(routes);

module.exports = server => server.use(routes);
